#include "awlib.h"

#include <search.h>
#include <stdlib.h>
#include <string.h>

// {{{ vector

Vec*
__vec_new(size_t sz, DESTRUCTOR destructor)
{
    if (sz != sizeof(void*) && destructor)  // no destructors for non-pointer types
        abort();

    Vec* vec = calloc(1, sizeof(Vec));
    vec->type_sz = sz;
    vec->destructor = destructor;
    return vec;
}

void
vec_destroy(Vec** vec)
{
    if ((*vec)->destructor) {
        void* tmp;
        vec_iter_mut(*vec, void*, tmp)
            (*vec)->destructor(tmp);
    }

    free((*vec)->data);
    free(*vec);
}

void
vec_expand(Vec* vec)
{
    if (vec->count == vec->capacity)
        vec->capacity = vec->capacity * 2 + 1;
    ++vec->count;
}

size_t
vec_count(const Vec* vec)
{
    return vec->count;
}

void
vec_push_ptr(Vec* vec, const void* ptr)
{
    assert(vec->type_sz == sizeof(void*));
    vec_expand(vec);
    vec->data = realloc((vec)->data, ((vec)->capacity * sizeof(void*)));
    vec->data[vec->count - 1] = ptr;
}

void
vec_insert_ptr(Vec* vec, size_t n, const void* ptr)
{
    assert(vec->type_sz == sizeof(void*));
    assert(n < vec->count);

    vec_expand(vec);
    vec->data = realloc((vec)->data, ((vec)->capacity * sizeof(void*)));

    if (n < (vec->count - 1))
        memmove(&vec->data[n+1], &vec->data[n], (vec->count - n - 1) * vec->type_sz);

    vec->data[n] = ptr;
}

void
vec_remove(Vec* vec, size_t n)
{
    assert(n < vec->count);
    if (vec->destructor)
        vec->destructor(vec_at_mut(vec, void*, n));
    if (n < (vec->count - 1))
        memmove(&vec->data[n], &vec->data[n+1], (vec->count - n - 1) * vec->type_sz);

    --vec->count;
}

void
vec_pop(Vec* vec)
{
    assert(vec_count(vec) > 0);
    vec_remove(vec, vec_count(vec) - 1);
}

// }}}

// {{{ sets

typedef struct Set {
    size_t     type_sz;
    void*      root;
    COMPARE    compare;
    DESTRUCTOR destructor;
} Set;

Set*
__set_new(size_t sz, COMPARE compare, DESTRUCTOR destructor)
{
    Set* set = calloc(1, sizeof(Set));
    set->type_sz = sz;
    set->root = NULL;
    set->compare = compare;
    set->destructor = destructor;
    return set;
}

void
set_destroy(Set** set)
{
    // TODO - destroy elements
    free(*set);
}

size_t
set_count(Set* set)
{
    return 0;
}

static int ptr_compare(const void* p1, const void* p2)
{
    return p1 < p2;
}

void
set_insert_ptr(Set* set, const void* ptr)
{
    tsearch(ptr, &set->root, set->compare ? set->compare : ptr_compare);
}

// }}}
