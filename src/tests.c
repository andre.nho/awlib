#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "awlib.h"

// {{{ test framework

typedef const char* (*tf)(void);

static int count = 0;

#define ENSURE(code) {  \
    ++count;            \
    if (!(code))        \
        abort();        \
    else                \
        printf(".");    \
}

#define T(name) #name, name

static void
test(const char* name, ...)
{
    printf("#\n# %s\n#\n", name);

    va_list ap;
    va_start(ap, name);

    const char* desc;
    while((desc = va_arg(ap, const char*)) != NULL) {
        printf("[%-15s] ", desc);
        va_arg(ap, void(*)(void))();
        printf("\n");
    }

    va_end(ap);
}

// }}}

// {{{ framework

static void
basic(void)
{
    ENSURE(1 == 1);
    // ENSURE_FAIL(abort());
}

// }}}

// {{{ vectors

typedef struct P { int x, y; } P;

static void
vec_leaks(void)
{
    P* p1 = calloc(1, sizeof(P)); *p1 = (P) { 1, 2 };

    Vec* vec = vec_new(P*, free);
    vec_push_ptr(vec, p1);
    vec_destroy(&vec);
}

static void
vec_ptr(void)
{
    Vec* vec = vec_new(P*, free);

    P* p1 = calloc(1, sizeof(P)); *p1 = (P) { 1, 2 };
    P* p2 = calloc(1, sizeof(P)); *p2 = (P) { 3, 4 };
    P* p3 = calloc(1, sizeof(P)); *p3 = (P) { 5, 6 };

    vec_push_ptr(vec, p1);
    vec_push_ptr(vec, p2);
    vec_push_ptr(vec, p3);

    ENSURE(vec_count(vec) == 3);
    ENSURE(vec_at(vec, P*, 0)->x == 1);
    ENSURE(vec_at(vec, P*, 0)->y == 2);
    ENSURE(vec_at(vec, P*, 2)->x == 5);
    ENSURE(vec_at(vec, P*, 2)->y == 6);

    const P* p;
    int i = 0;
    vec_iter(vec, const P*, p) {
        if (i == 0)
            ENSURE(p->x == 1);
        if (i == 2)
            ENSURE(p->y == 6);
        ++i;
    }

    vec_at_mut(vec, P*, 0)->x = 42;
    ENSURE(vec_at(vec, P*, 0)->x == 42);

    vec_remove(vec, 1);
    ENSURE(vec_count(vec) == 2);
    ENSURE(vec_at(vec, P*, 1)->x == 5);
    ENSURE(vec_at(vec, P*, 1)->y == 6);

    P* p4 = calloc(1, sizeof(P)); *p4 = (P) { 33, 44 };
    vec_insert_ptr(vec, 1, p4);
    ENSURE(vec_count(vec) == 3);
    ENSURE(vec_at(vec, P*, 1)->x == 33);
    ENSURE(vec_at(vec, P*, 1)->y == 44);
    ENSURE(vec_at(vec, P*, 2)->x == 5);
    ENSURE(vec_at(vec, P*, 2)->y == 6);

    vec_destroy(&vec);
}

static void
vec_string(void)
{
    Vec* vec = vec_new(const char*, NULL);

    const char *p1 = "hello",
               *p2 = "world",
               *p3 = "test";

    vec_push_ptr(vec, p1);
    vec_push_ptr(vec, p2);
    vec_push_ptr(vec, p3);

    ENSURE(vec_count(vec) == 3);
    ENSURE(strcmp(vec_at(vec, const char*, 0), "hello") == 0);
    ENSURE(strcmp(vec_at(vec, const char*, 2), "test") == 0);

    vec_destroy(&vec);
}

static void
vec_struct(void)
{
    Vec* vec = vec_new(P*, NULL);

    P p1 = { 1, 2 };
    P p2 = { 3, 4 };

    vec_push_n(vec, p1);
    vec_push_n(vec, p2);
    vec_push_n(vec, ((P) { 5, 6 }));

    ENSURE(vec_count(vec) == 3);
    ENSURE(vec_at(vec, P, 0).x == 1);
    ENSURE(vec_at(vec, P, 0).y == 2);
    ENSURE(vec_at(vec, P, 2).x == 5);
    ENSURE(vec_at(vec, P, 2).y == 6);

    P p;
    int i = 0;
    vec_iter(vec, P, p) {
        if (i == 0)
            ENSURE(p.x == 1);
        if (i == 2)
            ENSURE(p.y == 6);
        ++i;
    }

    vec_insert_n(vec, 1, ((P) { 33, 44 }));
    ENSURE(vec_count(vec) == 4);
    ENSURE(vec_at(vec, P, 0).x == 1);
    ENSURE(vec_at(vec, P, 0).y == 2);
    ENSURE(vec_at(vec, P, 1).x == 33);
    ENSURE(vec_at(vec, P, 1).y == 44);
    ENSURE(vec_at(vec, P, 3).x == 5);
    ENSURE(vec_at(vec, P, 3).y == 6);

    vec_destroy(&vec);
}

static void
vec_num(void)
{
    Vec* vec = vec_new(int, NULL);

    int p1 = 1;

    vec_push_n(vec, p1);
    vec_push_n(vec, 2);
    vec_push_n(vec, 3);

    ENSURE(vec_count(vec) == 3);
    ENSURE(vec_at(vec, int, 0) == 1);
    ENSURE(vec_at(vec, int, 2) == 3);

    vec_destroy(&vec);
}

// }}}

// {{{ set

static void
set_ptr(void)
{
    Set* set = set_new(P*, NULL, free);

    P* p1 = calloc(1, sizeof(P)); *p1 = (P) { 1, 2 };
    P* p2 = calloc(1, sizeof(P)); *p2 = (P) { 3, 4 };
    P* p3 = calloc(1, sizeof(P)); *p3 = (P) { 5, 6 };

    set_insert_ptr(set, p1);
    set_insert_ptr(set, p2);
    set_insert_ptr(set, p3);

    set_destroy(&set);
}

static int p_compare(const void* p1, const void* p2)
{
    return ((const P*)p1)->x + ((const P*)p1)->y * 10000
         < ((const P*)p2)->x + ((const P*)p2)->y * 10000;
}

static void
set_struct(void)
{
    Set* set = set_new(P*, p_compare, free);

    P p1 = { 1, 2 };
    P p2 = { 3, 4 };

    set_insert_n(set, P, p1);
    set_insert_n(set, P, p2);
    set_insert_n(set, P, ((P) { 5, 6 }));

    set_destroy(&set);
}

static void
set_n(void)
{
    Set* set = set_new(P*, NULL, free);

    int p1 = 1, p2 = 2;

    set_insert_n(set, int, p1);
    set_insert_n(set, int, p2);
    set_insert_n(set, int, 3);

    set_destroy(&set);
}

// }}}

int main(void)
{
    test("framework", T(basic), NULL);
    test("vector", T(vec_leaks), T(vec_ptr), T(vec_string), T(vec_struct), T(vec_num), NULL);
    test("set", T(set_ptr), T(set_struct), T(set_n), NULL);
}
