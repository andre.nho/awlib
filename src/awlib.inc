#include <stdint.h>

// {{{ vector

typedef struct Vec {
    size_t       type_sz;
    const void** data;
    size_t       count;
    size_t       capacity;
    DESTRUCTOR   destructor;
} Vec;

Vec* __vec_new(size_t sz, DESTRUCTOR destructor);

extern void vec_expand(Vec* vec);

#define vec_push_n(vec, value) {                                                     \
    assert(sizeof(value) == vec->type_sz);                                           \
    vec_expand(vec);                                                                 \
    vec->data = realloc((vec)->data, ((vec)->capacity * sizeof(__typeof__(value)))); \
    ((__typeof__(value)*)(vec)->data)[vec->count - 1] = value;                       \
}

#define vec_insert_n(vec, n, value) {                                                 \
    assert(sizeof(value) == vec->type_sz);                                            \
    assert(n < vec->count);                                                           \
    vec_expand(vec);                                                                  \
    vec->data = realloc((vec)->data, ((vec)->capacity * sizeof(__typeof__(value))));  \
    if (n < (vec->count - 1))                                                         \
        memmove(&vec->data[n+1], &vec->data[n], (vec->count - n - 1) * vec->type_sz); \
    ((__typeof__(value)*)(vec)->data)[n] = value;                                     \
}

#define vec_at(vec, T, n)       (assert(n < vec->count),              \
                                 assert(sizeof(T) == (vec)->type_sz), \
                                 ((const T*)(vec)->data)[(n)])

#define vec_at_mut(vec, T, n)    (assert(n < vec->count),              \
                                 assert(sizeof(T) == (vec)->type_sz), \
                                 ((T*)(uintptr_t /* remove constness without warning */)(vec)->data)[(n)])

#define vec_iter(vec, T, item)          for (size_t __i = 0; \
                                             item = vec_at((vec), T, (__i < vec_count(vec) ? __i : 0)), __i < vec_count(vec);\
                                             ++__i)
       
#define vec_iter_mut(vec, T, item)      for (size_t __i = 0; \
                                             item = vec_at_mut((vec), T, (__i < vec_count(vec) ? __i : 0)), __i < vec_count(vec);\
                                             ++__i)
      
// }}}

// {{{ set

typedef int (*COMPARE)(const void*, const void*);

typedef struct Set Set;

Set* __set_new(size_t sz, COMPARE compare, DESTRUCTOR destructor);

#define set_insert_n(set, T, value) {   \
    T* tmp = calloc(1, sizeof(T));      \
    *tmp = (value);                     \
    set_insert_ptr((set), tmp);         \
}

// }}}
