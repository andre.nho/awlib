#ifndef AWLIB_H_
#define AWLIB_H_

#include <assert.h>
#include <stdlib.h>

typedef void (*DESTRUCTOR)(void*);

#include "awlib.inc"

// 
// MACROS
//

// {{{ UNUSED(x)  protect from compiler warnings when a parameter is not used

#if defined(__GNUC__)
#define UNUSED(x)       x##_UNUSED __attribute__((unused))
#else
#define UNUSED(x)       x##_UNUSED
#endif

// }}}

//
// VECTOR
//

typedef struct Vec Vec;

#define vec_new(T, destructor)           (__vec_new(sizeof(T), destructor))
void    vec_destroy(Vec** vec);

size_t  vec_count(const Vec* vec);

void    vec_push_ptr(Vec* vec, const void* ptr);
void    vec_insert_ptr(Vec* vec, size_t n, const void* ptr);
/*      
void    vec_push_n(Vec* vec, T value);
void    vec_insert_n(Vec* vec, T value);

const T vec_at(Vec* vec, const T, size_t pos);
T       vec_at_mut(Vec* vec, T, size_t pos);
        
        vec_iter(Vec* vec, const T, const T var) {}
        vec_iter_mut(Vec* vec, T, T var) {}
*/

void    vec_remove(Vec* vec, size_t n);
void    vec_pop(Vec* vec);

// 
// SET
//

typedef struct Set Set;

typedef int (*COMPARE)(const void*, const void*);

#define set_new(T, compare, destructor)        (__set_new(sizeof(T), compare, destructor))
void    set_destroy(Set** set);

size_t  set_count(Set* set);

void    set_insert_ptr(Set* set, const void* ptr);
/*
void    set_insert_n(Set* set, T value);
*/

#endif
