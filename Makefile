#
# software version
#
VERSION = 0.3
CC = gcc

#
# files
#
VPATH = src
OBJS := awlib.o tests.o

# compilation options
#
CFLAGS = -std=c11
CPPFLAGS = -DVERSION=\"$(VERSION)\" -I.

#
# add cflags/libraries
#
ifneq ($(OS),Windows_NT)
  LDFLAGS += -fuse-ld=gold 
endif

# 
# add warnings
#
WARNINGS=@src/WARNINGS

# 
# debug/release
# 
all: awlib_tests
	@echo Choose a target: 'debug' or 'release'.

CFLAGS += -g3 -O0 -DDEBUG -fno-inline-functions -ggdb3 

check: awlib_tests
	@./awlib_tests

#
# pull dependence info from existing .o files
#
-include $(OBJS:.o=.d)

# 
# compile source files
#
%.o: %.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $(WARNINGS) $(TARGET_CFLAGS) $< -o $@
	@$(CC) -MM $(CPPFLAGS) $(CFLAGS) $< > $*.d
	@mv -f $*.d $*.d.tmp
	@sed -e 's|.*:|$*.o:|' < $*.d.tmp > $*.d
	@sed -e 's/.*://' -e 's/\\$$//' < $*.d.tmp | fmt -1 | sed -e 's/^ *//' -e 's/$$/:/' >> $*.d
	@rm -f $*.d.tmp

#
# link
#
awlib_tests: $(OBJS) src/tests.o
	$(CC) $^ -o $@ $(TARGET_LDFLAGS) $(LDFLAGS)

#
# other rules
#
cloc:
	cloc Makefile src/*.h src/*.c

lint:
	clang-tidy src/*.h src/*.c "-checks=*" -- -I. --std=c11 -DVERSION=\"$(VERSION)\"

check-leaks: awlib_tests
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes ./awlib_tests

clean:
	rm -f awlib_tests *.o *.d **/*.o **/*.d gmon.out **/**/*.mo

.PHONY: cloc lint check-leaks clean
