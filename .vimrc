" files format
set fileformat=unix
set ts=4
set sw=4
set sts=4
set expandtab

" visual
set foldmethod=marker

" make program
set makeprg=clear\ \&\&\ make\ debug
nnoremap <Leader>R :!make -j4 debug && ./flipflop<cr>
nnoremap <Leader>G :!git commit -a && git push<CR>

"au FileType cpp syn keyword Type Test

" open all files
args src/awlib.h
args src/awlib.c
args src/tests.c
args Makefile
args .vimrc

b 1   " open initial buffer

" open specific buffers
map <Leader>M :b Makefile<CR>
map <Leader>t :b src/tests.c<CR>
map <Leader>h :b src/awlib.h<CR>
map <Leader>c :b src/awlib.c<CR>
map <Leader>V :b .vimrc<CR>

" swap between cc and hh                                                                                                               
function! SwapBuffers()
  if expand("%:e") == "c"
    exe "b" fnameescape(expand("%:r").".h")
  elseif expand("%:e") == "h"
    exe "b" fnameescape(expand("%:r").".c")
  endif
endfunction
map <Leader>.  :call SwapBuffers()<CR>
map  <ESC>; :call SwapBuffers()<CR>
imap <ESC>; <ESC>:call SwapBuffers()<CR>
